function filter(elements, cb) {
  if (!Array.isArray(elements) || cb === undefined || elements.length === 0) {
    return elements;
  } else if (cb !== undefined && typeof cb !== "function") {
    return "Callback must be a function";
  }
  let filteredArray = [];
  for (let i in elements) {
    let k = elements[i];
    let res = cb(k);
    if (res) {
      filteredArray.push(k);
    }
  }
  return filteredArray;
}

module.exports = filter;
