let find = require("../find.cjs");

function even(num) {
  return num % 2 === 0;
}

let elements = [1, 2, 3, 4, 5, 6];

console.log(find(null));
console.log(find(undefined));
console.log(find([]));
console.log(find(""));
console.log(find(elements, even));
console.log(find("Hello"));
console.log(find({ a: 2, b: 3 }, even));
console.log(find(elements, ""));
