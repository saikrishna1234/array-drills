let filter = require("../filter.cjs");

let array = [1, 2, 3, 4, 5, 6];

function getFiltered(num) {
  return num % 2 === 0;
}

console.log(filter(null));
console.log(filter(undefined));
console.log(filter([]));
console.log(filter(""));
console.log(filter(array, getFiltered));
console.log(filter(array));
console.log(filter([1, 2, 3], [1, 2, 3]));