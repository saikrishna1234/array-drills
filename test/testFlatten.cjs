let flatten = require("../flatten.cjs");

console.log(flatten(null));
console.log(flatten(undefined));
console.log(flatten([]))
console.log(flatten(""));
console.log(flatten([1, [2], [3, [[4]]]]));