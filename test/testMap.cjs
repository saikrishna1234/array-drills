let map = require("../map.cjs");

function multiply(num) {
  return num * 3;
}

let array = [1, 1, 2, 3, 4, 5];

console.log(map(null));
console.log(map(undefined));
console.log(map([]));
console.log(map(""));
console.log(map(array, multiply));
console.log(map({ a: 2, b: 3 }, multiply));
console.log(map([1, 2, 3], ""));
