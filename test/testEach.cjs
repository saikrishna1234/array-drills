let each = require("../each.cjs");

function alert(element, index, list) {
  console.log(`value: ${element} index: ${index} array: ${list}`);
}

console.log(each(null));
console.log(each(undefined));
console.log(each([]));
console.log(each(""));
each([1, 2, 3], alert);
each({ a: 1, b: 2, c: 3 }, alert);
console.log(each([1, 2, 3], "123"));
