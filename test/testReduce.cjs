let reduce = require("../reduce.cjs");

let list = [1, 2, 3];

function addition(accumulator, value) {
  return accumulator + value;
}

function maximum(accumulator, value) {
  if (accumulator > value) {
    return accumulator;
  } else {
    return value;
  }
}

console.log(reduce(undefined));
console.log(reduce(null));
console.log(reduce(""));
console.log(reduce(list, addition, 10));
console.log(reduce(list, addition));
console.log(reduce(list, maximum));
