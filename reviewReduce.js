function reduce(elements, cb, startingValue = null) {
  if (!Array.isArray(elements) || typeof cb !== "function") {
    return undefined;
  }
  let i = 0;
  if (startingValue === null) {
    startingValue = elements[0];
    i++;
  }
  for (i; i < elements.length; i++) {
    startingValue = cb(startingValue, elements[i]);
  }
  return startingValue;
}

let list = [1, 2, 3];

function maximum(accumulator, value) {
  if (accumulator > value) {
    return accumulator;
  } else {
    return value;
  }
}

function addition(accumulator, value) {
  return accumulator + value;
}

console.log(reduce(undefined));
console.log(reduce(null));
console.log(reduce(""));
console.log(reduce(list, addition));
console.log(reduce(list, maximum, 8));
