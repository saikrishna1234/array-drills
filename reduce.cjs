function reduce(list, cb, startingValue) {
  if (!Array.isArray(list) || typeof cb !== "function") {
    return undefined;
  }

  let res = 0;
  for (let i in list) {
    let k = list[i];
    res = cb(res, k);
  }
  return startingValue === undefined ? res : res + startingValue;
}

module.exports = reduce;
