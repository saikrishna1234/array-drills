function each(list, cb) {
  let newArray = [];
  if (cb !== undefined && typeof cb !== "function") {
    return "Callback must be a function";
  } else if (typeof list !== "object" || list === null || list.length === 0) {
    return list;
  }
  for (let key in list) {
    k = cb(list[key], key, list);
    newArray[key] = k;
  }
  return newArray;
}

module.exports = each;
