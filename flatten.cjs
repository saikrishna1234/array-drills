function flatten(elements) {
  if (elements === undefined || elements === null) {
    return [];
  }
  let newArray = [];

  for (let i = 0; i < elements.length; i++) {
    if (Array.isArray(elements[i])) {
      let k = flatten(elements[i]);
      newArray = newArray.concat(k);
    } else {
      newArray.push(elements[i]);
    }
  }
  return newArray;
}

module.exports = flatten;
