function find(elements, cb) {
  if (cb !== undefined && typeof cb !== "function") {
    return "Callback must be a function";
  }
  if (typeof elements === "string" && elements.length > 0) {
    return elements[0];
  }
  for (let i in elements) {
    if (cb(elements[i])) {
      return elements[i];
    } else {
      return undefined;
    }
  }
}

module.exports = find;
