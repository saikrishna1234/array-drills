function map(arr, cb) {
  if (cb !== undefined && typeof cb !== "function") {
    return "Callback must be a function";
  }
  let newArray = [];
  for (let i in arr) {
    newArray.push(cb(arr[i]));
  }
  return newArray;
}

module.exports = map;
